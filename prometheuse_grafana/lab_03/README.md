# Prometheus and Grafana with Docker
# Lab 03: Prometheus metrics and Grafana Dashboard


# Preparations
- Verify that prometheuse and node exporter and grafana are up and running:
    - Browse to http://serverip_or_hostname:9090
    - Browse to http://serverip_or_hostname:9100
    - Browse to http://serverip_or_hostname:3000

---

# Tasks

 - Prometheus metrics

 - Node exporter metrics

 - Grafana Dashboard

 - compare metrix to actual Memory load

--- 
 
## Prometheus metrics 

&nbsp;

- To get list of metrics browse to: ```http://serverip_or_hostname:9090/metrics```
- list of metrics accomapnied with theyre desciption

<img alt="Image 1" src="images/prometheus_1.png" width="100%" height="100%">

&nbsp;

- Graph for metric: ```node_memory_MemAvailable_bytes```
- Add ```node_memory_MemAvailable_bytes``` to the expression box
- select Graph 
- select Execute

<img alt="Image 2" src="images/prometheus_2.png" width="100%" height="100%">


## Node Exporter metrics

- Review node exporter metrics
- Make sure to change <server ip> with the actual server ip address you are using

```

Browse to http://serverip_or_hostname:9100/metrics
search for metric node_cpu_seconds_total
  Answer:
  How many modes does node exporter collect ?

search for metric node_memory_MemAvailable_bytes
  Answer:
  is this he ONLY memory metric Node Exporter collect ?


```


## Grafana Dashboard
- Navigate to grafana Home - Add new dashboard:

<img alt="Image 3" src="images/grafana_1.png" width="75%" height="100%">
<img alt="Image 4" src="images/grafana_2.png" width="75%" height="100%">

- Add metric: 
```
node_memory_MemAvailable_bytes / 1024^3
``` 
to the data source editor and save
- We devide by 1024^3 As we want to normalize the metric value in Bytes to Giga
- Add 
```
node_memory_MemAvailable_bytes in GigaBytes
```
 in panel title

<img alt="Image 5" src="images/grafana_3.png" width="75%" height="100%">
<img alt="Image 6" src="images/grafana_4.png" width="75%" height="100%">
<img alt="Image 6" src="images/grafana_5.png" width="75%" height="100%">

## Compare to Actual memory on the host
- Run command:
```
top
```
- results will looks like:

```
  top - 00:16:23 up 3 days, 16:34,  5 users,  load average: 0.00, 0.00, 0.00
  Tasks: 140 total,   1 running,  86 sleeping,  14 stopped,   0 zombie
  %Cpu(s):  0.5 us,  0.2 sy,  0.0 ni, 99.3 id,  0.0 wa,  0.0 hi,  0.0 si,  0.0 st
  KiB Mem :  7652344 total,  3689796 free,   723172 used,  3239376 buff/cache
  KiB Swap:        0 total,        0 free,        0 used.  6623984 avail Mem

```

## Compare memory from top command to prometheuse metric

Analyze the memory usage in the line start with "KiB Mem"
Calculate the amount of : free + buff + cashed
Compare it

TO

node_memory_MemAvailable_bytes metric
