# Prometheus and Grafana using Docker
# This demo include all needed files to create Prometheus and Grafana using Docker.

In order to run this demo:

- Make sure you have docker and doker compose installed
- Clone this folder and run commands:
- docker compose up -d

## Prometheuse list of metrics

- Browse to http://serverip_or_hostname:9090   
- To get list of metrics browse to: http://serverip_or_hostname:9090/metrics
- Detailed instructions in lab_03

### Grafana Dashboard

 - Browse to http://serverip_or_hostname:3000
    - user: admin password: admin
 - Navigate to grafana Home - Add new dashboard
 - Add metric: node_memory_MemAvailable_bytes / 1024^3 to data source editor and save
 - Detailed instructions in lab_03


# Additionals 
This is the output of the command docker compose up -d:

$ docker compose up -d
&nbsp;

```
[+] Running 4/4

  ✔ Network demo_code_default            Created                                 0.0s  
  ✔ Container demo_code-prometheus-1     Started                                 0.8s
  ✔ Container monitoring_node_exporter    Started                                0.7s  
  ✔ Container demo_code-grafana-1         Started                                1.2s 
```



