# Prometheus and Grafana with Docker
# Lab 01: Create Prometheus and Grafana configuration files

---

# Preparations
 - We will install prometheus and grafana on 2 separate containers using docker and docker-compose

 - We will demonstrate metricsses coming from the host
  
 - Get monitoring server IP (ask the instructor), OR work on your local PC
 
 - Credentials: 
 username:**sela** , password:**sela**

---

# Tasks

 - Make sure docker, docker-compose and curl are installed
 
 - Create persistent volume
 
 - Create  prometheus configuration file

 - Create  grafana configuration and datasource files

 - Create  docker-compose configuration file


--- 
 
## Make sure docker, docker-compose and curl are installed

&nbsp;

 - Inside the server open bash (git bash on windows) and run commands:

```
docker version
docker-compose version
curl --version

```
<img alt="Image 1.1" src="images/verification.png" width="100%" height="100%">

&nbsp;

 - Create persistent volume, run command:
 
```
mkdir -p ~/prometheus-grafana/{grafana,prometheus,alertmanager}

```

&nbsp;

 - Create  prometheus configuration file
 - run command:
 ```

tee ~/prometheus-grafana/prometheus/prometheus.yml<<EOF
global:
  scrape_interval: 15s
  scrape_timeout: 10s
  evaluation_interval: 15s

alerting:
  alertmanagers:
  - static_configs:
    - targets: ['alertmanager:9093']  # Update this with your Alertmanager address
    scheme: http
    timeout: 10s
    api_version: v2
rule_files:
  - "alert.rules.yml" 
scrape_configs:
  - job_name: 'prometheus'
    honor_timestamps: true
    metrics_path: /metrics
    static_configs:
    - targets: ['prometheus:9090']
  - job_name: 'node'
    honor_timestamps: true
    metrics_path: /metrics
    static_configs:
    - targets: ['<server ip>:9100']
EOF

```
Make sure to change <server ip> with the actual server ip address you are using:
Run:
```
nano ~/prometheus-grafana/prometheus/prometheus.yml
```
and replace the <server ip> with the server ip address.

&nbsp;

<img alt="Image 1.2" src="images/prometheuse_config_yml.png" width="100%" height="100%">


- Create Alert rule configuration file 

```

tee ~/prometheus-grafana/prometheus/alert.rules.yml<<EOF
groups:
- name: cpu_alerts
  rules:
  - alert: HighCpuUsage
    expr: 100 - (avg by (instance) (irate(node_cpu_seconds_total{mode="idle"}[1m])) * 100) > 60
    for: 1m
    labels:
      severity: warning
    annotations:
      summary: High CPU usage detected
      description: CPU usage is above 80% on {{ $labels.instance }}.
EOF

```

&nbsp;


- Create  grafana configuration file, run command:

```
  curl  https://raw.githubusercontent.com/grafana/grafana/v7.5.1/conf/defaults.ini -o ~/prometheus-grafana/grafana/grafana.ini

```

&nbsp;

- Create  grafana configuration datasource file, run command:
```

tee ~/prometheus-grafana/grafana/datasource.yml<<EOF
apiVersion: 1
datasources:
- name: Prometheus
  type: prometheus
  url: http://<server ip>:9090 
  isDefault: true
  access: direct
  editable: true
EOF

```
Make sure to change <server ip> with the actual server ip address you are using:
Run:
```
nano ~/prometheus-grafana/grafana/datasource.yml
```
and replace the <server ip> with the server ip address.

&nbsp;

<img alt="Image 1.2" src="images/grafana_datasource.png" width="100%" height="100%">


- Create Alert manager configuration file 

```
tee ~/prometheus-grafana/alertmanager/alertmanager.yml<<EOF
global:
  resolve_timeout: 5s
route:
  group_by: ['alertname']
  group_wait: 10s
  group_interval: 10s
  repeat_interval: 1h
  receiver: 'null'
receivers:
- name: 'null'
EOF

```


- Download node exporter
- run commands

```
  curl -LJO https://github.com/prometheus/node_exporter/releases/download/v1.6.1/node_exporter-1.6.1.linux-amd64.tar.gz
  tar -xzvf node_exporter-1.6.1.linux-amd64.tar.gz

```

```
  cd node_exporter-1.6.1.linux-amd64/
  ./node_exporter &
  <ENTER>
  cd 

```
