# Prometheus and Grafana with Docker
# This folder contains 3 labs:

- Lab 01: Create Prometheus and Grafana configuration files
 
- Lab 02: Create  docker-compose configuration filea and run the application

- Lab 03: Prometheus metrics and Grafana Dashboard

- Lab 04: Prometheus Alert Manager

- demo_code: All files prepared for demo
