# Prometheus and Grafana with Docker
# Lab 02: Create  docker-compose configuration files and run the application


# Preparations
 - Verify that these files exist:
    - ~/prometheus-grafana/prometheus/prometheus.yml
    - ~/prometheus-grafana/grafana/grafana.ini 
    - ~/prometheus-grafana/grafana/datasource.yml
    
---

<img alt="Image 1.1" src="images/verification.png" width="100%" height="100%">

# Tasks

 - Create  docker-compose configuration file

 - Create containers using docker-compose 

 - run the applications


--- 
 
## Create  docker-compose configuration file

&nbsp;

- run command:

```
tee ~/prometheus-grafana/docker-compose.yml << EOF
version: "3.7"
services:
  prometheus:
    image: prom/prometheus:v2.51.0
    container_name: prometheus
    volumes:
      -  ~/prometheus-grafana/prometheus/prometheus.yml:/etc/prometheus/prometheus.yml
      - ~/prometheus-grafana/prometheus/alert.rules.yml:/etc/prometheus/alert.rules.yml # Mount the alert rules file
    ports:
      - 9090:9090
  alertmanager:
    image: prom/alertmanager:v0.27.0
    container_name: alertmanager
    volumes:
      - ~/prometheus-grafana/alertmanager/alertmanager.yml:/etc/alertmanager/alertmanager.yml # Mount the Alertmanager config
    ports:
      - 9093:9093
  grafana:
    image: grafana/grafana:7.5.17
    container_name: prometheus_grafana
    volumes:
      -  ~/prometheus-grafana/grafana/grafana.ini:/etc/grafana/grafana.ini
      -  ~/prometheus-grafana/grafana/datasource.yml:/etc/grafana/provisioning/datasources/datasource.yaml
    ports:
      - 3000:3000
    links:
      - prometheus
EOF

```

## Create containers using docker-compose 

&nbsp;

- Navigate to data directory, run commands:

```

cd  ~/prometheus-grafana/

```

&nbsp;

- Run the applications with docker-compose, run commands:

```

docker-compose up -d

```
<img alt="Image 1.1" src="images/verify_docker_compose.png" width="100%" height="100%">

&nbsp;

## Verify output:

- run commands:

```
 docker ps
```
<img alt="Image 1.1" src="images/verify_docker_compose_2.png" width="100%" height="100%">

&nbsp;

- verify ports and names of the containers to be 

```
PORTS   NAMES
0.0.0.0:3000->3000/tcp   prometheus_grafana
0.0.0.0:9090->9090/tcp   prometheus
0.0.0.0:9093->9093/tcp   alertmanager

```

## run the applications

- Prometheus: open webbeowser and navigate to: http://serverip_or_hostname:9090 
- navigate to status-> targets, you should be able to see you targets

<img alt="Image 1.1" src="images/verify_prometheus.png" width="100%" height="100%">

- Verify that there are 2 targets: node and Prometheus 
- Verify that both of them are up

<img alt="Image 1.1" src="images/verify_prometheus_targets.png" width="100%" height="100%">

- Grafana: open webbeowser and navigate to: http://serverip_or_hostname:3000 
- username: admin, password: admin

<img alt="Image 1.1" src="images/verify_grafana_1.png" width="100%" height="100%">
<img alt="Image 1.1" src="images/verify_grafana_2.png" width="100%" height="100%">
<img alt="Image 1.1" src="images/verify_grafana_3.png" width="100%" height="100%">

- 
```
Browse to http://serverip_or_hostname:9100
Veirfy that you get "Node Exporter"

```
<img alt="Image 1.1" src="images/node_exporter.png" width="100%" height="100%">

