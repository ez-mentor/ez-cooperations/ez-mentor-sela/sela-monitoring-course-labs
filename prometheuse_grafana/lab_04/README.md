# Prometheus and Grafana with Docker
# Lab 04: Prometheus Alerts & Alertmanager 


# Preparations
- Verify that prometheuse and grafana and alertmanager are up and running:
    - Browse to http://serverip_or_hostname:9090 
    - Browse to http://serverip_or_hostname:3000
    - Browse to http://serverip_or_hostname:9093

---

# Tasks

 - Create 'HighCpuUsage' alert on node-exporter By loading the CPU

 - Review alert on Prometheus

 - Review alert on alertmanager

 - Create alerts Grafana Dashboard 



--- 
 
## Create 'HighCpuUsage' alert on node-exporter By loading the CPU

&nbsp;

- install stress - run below commands:
```
  apt-get update
  apt-get install stress
```

- run below command on the server (<ip address>)
```
  stress --cpu `nproc` --timeout 4m
          
```


&nbsp;

## Review alert on Prometheus

- Browse to  http://serverip_or_hostname:9090/
- Select Alerts in top banner 
- Review the alert details

<img alt="Image 1" src="images/prometheus_alerts_1.png" width="75%" height="100%">

&nbsp;


## Review alert on alertmanager

- Browse to  http://serverip_or_hostname:9093/
- Review the alert details

<img alt="Image 2" src="images/alertmanager_1.png" width="75%" height="100%">

- Click on silence 

<img alt="Image 3" src="images/alertmanager_2.png" width="75%" height="100%">

- Fill creator and comment (In real life, we dont want to get this alert for some time)

<img alt="Image 4" src="images/alertmanager_3.png" width="75%" height="100%">

- Verify alert silence details

<img alt="Image 5" src="images/alertmanager_4.png" width="75%" height="100%">

- Go back to Alerts and verify that the laert is not there

<img alt="Image 6" src="images/alertmanager_5.png" width="75%" height="100%">


## Grafana Dashboard
- Navigate to grafana Home - Add new dashboard:

<img alt="Image 7" src="images/grafana_1.png" width="75%" height="100%">
<img alt="Image 8" src="images/grafana_2.png" width="75%" height="100%">

- Add  ALERTS{} in data source editor query and save

<img alt="Image 9" src="images/grafana_3.png" width="75%" height="100%">

- Reduce time to Last 5 minutes and verify that no alert on the dashboard

<img alt="Image 10" src="images/grafana_4.png" width="75%" height="100%">
