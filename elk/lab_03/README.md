# ELK using Docker
# Lab 03: Create KIBANA Dashboard 


---

# Tasks

 - Login to KIBANA 

 - create 2 search filters for "Received data" and "NO Data received"

 - Create Kibana Dashboard with 2 widget "Received data" and "NO Data received"
 

---

### Login to KIBANA 

- Make sure to change <server-ip> with the actual server ip address you are using

- Browse to http://server-ip:5601/app/home#/

<img alt="Image 1.0" src="images/kibana_dashboards_discover_0.png" width="100%" height="100%">

### create 2 search filters for "Received data" and "NO Data received"

Steps are:

-  create index 
-  create Filter 
-  create Dashboard 


### Create Index Pattern on flask-logs*

- Navigate to Stack Management -> Index patterns -> Index Pattern

<img alt="Image 1.1" src="images/kibana_index_0.png" width="100%" height="100%">

- Add flask-logs* Indexes

<img alt="Image 1.2" src="images/kibana_index_1.png" width="100%" height="100%">

<img alt="Image 1.3" src="images/kibana_index_2.png" width="100%" height="100%">

<img alt="Image 1.4" src="images/kibana_index_3.png" width="100%" height="100%">


- Navigate to Kibana -> Discover

<img alt="Image 1.1.0" src="images/kibana_index_0_1.png" width="100%" height="100%">

-  Add Filter on: message "is one of" "Received data"

<img alt="Image 1.1" src="images/kibana_dashboards_filter_2.png" width="100%" height="100%">

- While in the filter, Select message attribute and ADD, Select time slot for This Week 

<img alt="Image 1.1" src="images/kibana_dashboards_filter_3.png" width="100%" height="100%">

- Save the Serach as "Received data Search"

<img alt="Image 1.1" src="images/kibana_dashboards_filter_4.png" width="100%" height="100%">


- Do the same for "NO Data received Search"

<img alt="Image 1.1" src="images/kibana_dashboards_filter_5.png" width="100%" height="100%">

<img alt="Image 1.1" src="images/kibana_dashboards_filter_6.png" width="100%" height="100%">


#### Create Dashboard with 2 widget "Received data Search " and "NO Data received Search"

- Navigate to Dashboard ->  Create Dashboard

<img alt="Image 1.1" src="images/create_dashboard_1.png" width="100%" height="100%">

<img alt="Image 1.1" src="images/create_dashboard_2.png" width="100%" height="100%">

- click on "Add an existing"

<img alt="Image 1.1" src="images/create_dashboard_3.png" width="100%" height="100%">


- Select "Received Data Search" and "NO Data received Search"

<img alt="Image 1.1" src="images/create_dashboard_4.png" width="100%" height="100%">


- Save Dashboard name : DATA_SEARCH

<img alt="Image 1.1" src="images/create_dashboard_5.png" width="100%" height="100%">


- Enjoy the new KIBANA Dashboard

<img alt="Image 1.1" src="images/create_dashboard_6.png" width="100%" height="100%">