from flask import Flask, request, jsonify
import logging
from datetime import datetime

app = Flask(__name__)

# Configure logging
logging.basicConfig(filename='/app/logs/app.log', level=logging.INFO,
                    format='%(asctime)s %(levelname)s: %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S')

@app.route('/data', methods=['POST'])
def data_endpoint():
    try:
        data = request.json
        if not data:
            app.logger.error(f"NO data Received ")

        name = data['name']
        age = data['age']
        app.logger.info(f"Received data: name={name}, age={age}")
        return jsonify({"message": "Data received"}), 200
    except (KeyError, TypeError) as e:
        error_message = f"Error processing request: {e}"
        app.logger.error(error_message)
        return jsonify({"error": error_message}), 400

@app.route('/', methods=['GET'])
def welcome_endpoint():
    try:
        app.logger.info(f"GET call was submitted")
        return jsonify({"message": "What Do you need? - Please send POST CALL to .. /data"}), 200
    except (KeyError, TypeError) as e:
        error_message = f"Error processing GET request : {e}"
        app.logger.error(error_message)
        return jsonify({"error": error_message}), 400

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5000)
