# ELK using Docker - Demo Environment 
- instructions to create ELK, FileBeat and sample web server files 
- instructions how to add kibana dashboard

---

## Preparations
 
 - Get monitoring server IP (ask the instructor), OR work on your local PC docker-desktop
 
 - Credentials for provided server:

 username:**sela** , password:**sela**

---

## Tasks

 - Make sure docker, docker-compose and curl are installed
 
 - Create ELK stuck using docker-compose 

 - Demonstrate Application log files

 - Demonstrate logstash log files

 - Demonstrate Elasticsearch data
 
 - create Kibana Dashboard


--- 
 
## Make sure docker, docker-compose and curl are installed


 - Inside the server open bash (git bash on windows) and run commands:

```
docker version
docker-compose version
curl --version

```
<img alt="Image 1.1" src="images/verification.png" width="100%" height="100%">

&nbsp;

### Create ELK stuck using docker-compose

 - in folder elk/demo_code
 - run command:
 
```
sudo chown 0:0 filebeat/filebeat.yml
sudo chmod go-w filebeat/filebeat.yml
sudo chown 0:0 esdata
docker-compose up

```
Please allow 3 minutes until logstash will be connected


&nbsp;

### Demonstrate Application log files

 - run command:
 
```
 curl -X POST -H "Content-Type: application/json" -d '{"age": 5784, "name": "world" }' http://localhost:5000/data
 curl -X POST -H "Content-Type: application/json" -d '{}' http://localhost:5000/data
 cat  applogs/app.log
```
 - Demonstrate that there many lines in application log file,
 - Demonstrate 2 lines:

```
2024-01-16 16:40:52 INFO: Received data: name=world, age=5784
2024-01-16 16:43:51 ERROR: NO data Received
```

<img alt="Image 1.1" src="images/application_logs_1.png" width="100%" height="100%">

&nbsp;

### Demonstrate logstash log files

- Explain that based on logstash config file, it will create index name "flask-logs-*"
- the below is form logstash config file:

```
output {
    elasticsearch {
        hosts => ["http://elasticsearch:9200"]
        index => "flask-logs-%{+YYYY.MM.dd}"
    }
    stdout {
        codec => rubydebug
    }
}

```

 - run command:
 
```
docker logs elk-logstash

```

- explain that each line from the log file is being cut to many fields in elasticsearch index
- as exist in logstash log file:
- the below is form logstash log file:

```
{
           "log" => {
          "file" => {
            "path" => "/var/log/pythonapp/app.log"
        },
        "offset" => 3575
    },
      "loglevel" => "INFO",
       "message" => [
        [0] "2024-01-17 16:44:08 INFO: 172.30.0.1 - - [17/Jan/2024 16:44:08] \"POST /data HTTP/1.1\" 200 -",
        [1] "172.30.0.1 - - [17/Jan/2024 16:44:08] \"POST /data HTTP/1.1\" 200 -"
    ],
          "host" => {
        "name" => "256fa493cf42"
    },
      "@version" => "1",
    "@timestamp" => 2024-01-17T16:44:08.000Z,
         "agent" => {
                "type" => "filebeat",
                "name" => "256fa493cf42",
                  "id" => "4832b9f7-e87a-4c2a-81ad-0182cb7d4041",
             "version" => "7.9.3",
        "ephemeral_id" => "3c826a7d-2839-4849-bd68-b40f1431cce4",
            "hostname" => "256fa493cf42"
    },
     "timestamp" => "2024-01-17 16:44:08",
          "tags" => [
        [0] "beats_input_codec_plain_applied"
    ],
           "ecs" => {
        "version" => "1.5.0"
    },
         "input" => {
        "type" => "log"
    }
}


```


### Demonstrate Elasticsearch data

- Browse to http://<server ip>:9200/
- Browse to http:/<server ip>:9200/_cluster/health

- Demonstrate that staus is yellow or green and it is ok

- Browse to http://<server ip>:9200/flask-logs*/_search?q=message:%22Received%20data%22

- Demonstrate that there is 1 hit 
" {"took":25,"timed_out":false,"_shards":{"total":2,"successful":2,"skipped":0,"failed":0},"hits":{"total":{"value":1,"relation":"eq"}" 

- in the image there are 22 hits, as I called 22 times...

<img alt="Image 1.1" src="images/elasticsearch_search_2.png" width="100%" height="100%">

- Discuss how can we search elasticsearch using time slot and other filtesrs, suggest UI tool

### Create Kibana Dashboard with 2 widget "Received data" and "NO Data received"
#### First we will create 2 search filters

- Run LAB3 steps