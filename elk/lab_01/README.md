# ELK using Docker
# Lab 01: Create ELK, FileBeat and sample web server files

---

# Tasks

 - Make sure docker, docker-compose and curl are installed

 - Enlarge swap size
 
 - Create persistent volume

 - Create application
 
 - Create  logstash and filebeat configuration file

 - Create application logs, elasticsearch data persistent volume

 - Create  docker-compose configuration file


---

## Preparations
  
 - Get monitoring server IP (ask the instructor), OR work on your local PC docker-desktop
 
 - Credentials for provided server:

 username:**docker** , password:**docker**


--- 
  
## Make sure docker, docker-compose and curl are installed

&nbsp;

 - Inside the server open bash (git bash on windows) and run commands:

```
docker version
docker-compose version
curl --version

```
<img alt="Image 1.1" src="images/verification.png" width="100%" height="100%">

&nbsp;

## Create persistent volume
- run command:
 
```
mkdir -p ~/elk/{esdata,logstash,application,applogs,filebeat}
sudo chown 0:0 ~/elk/esdata

```

&nbsp;

### Enlarge swap size 
- run command:

```

tee ~/elk/create_swap.sh<<EOF
#!/bin/bash

# Set the size of the swap file
SWAP_SIZE=2G
SWAP_FILE=/swapfile

# Step 1: Create a swap file
sudo fallocate -l \$SWAP_SIZE \$SWAP_FILE || sudo dd if=/dev/zero of=\$SWAP_FILE bs=1M count=\$((SWAP_SIZE*1024))

# Step 2: Secure the swap file by setting the correct permissions
sudo chmod 600 \$SWAP_FILE

# Step 3: Make the swap file
sudo mkswap \$SWAP_FILE

# Step 4: Enable the swap file
sudo swapon \$SWAP_FILE

# Adding swap file entry to /etc/fstab to make it permanent
echo "\$SWAP_FILE none swap sw 0 0" | sudo tee -a /etc/fstab

# Verify the swap space
sudo swapon --show
sudo free -m

echo "Swap file of size \$SWAP_SIZE has been created and activated."

EOF

```

- run command:

```
chmod +x ~/elk/create_swap.sh
sudo ~/elk/create_swap.sh

```

### Create application files
- run command:

```

tee ~/elk/application/app.py<<EOF
from flask import Flask, request, jsonify
import logging
from datetime import datetime

app = Flask(__name__)

# Configure logging
logging.basicConfig(filename='/app/logs/app.log', level=logging.INFO,
                    format='%(asctime)s %(levelname)s: %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S')

@app.route('/data', methods=['POST'])
def data_endpoint():
    try:
        data = request.json
        if not data:
            app.logger.error(f"NO data Received ")

        name = data['name']
        age = data['age']
        app.logger.info(f"Received data: name={name}, age={age}")
        return jsonify({"message": "Data received"}), 200
    except (KeyError, TypeError) as e:
        error_message = f"Error processing request: {e}"
        app.logger.error(error_message)
        return jsonify({"error": error_message}), 400

@app.route('/', methods=['GET'])
def welcome_endpoint():
    try:
        app.logger.info(f"GET call was submitted")
        return jsonify({"message": "What Do you need? - Please send POST CALL to .. /data"}), 200
    except (KeyError, TypeError) as e:
        error_message = f"Error processing GET request : {e}"
        app.logger.error(error_message)
        return jsonify({"error": error_message}), 400

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5000)

EOF

```

&nbsp;

- run command:

```

tee ~/elk/application/requirements.txt<<EOF
Flask==1.1.2
EOF

```

&nbsp;

### Create Dockerfile 
- run command

```

tee ~/elk/application/Dockerfile<<EOF
FROM python:3.6

# Create and activate a virtual environment
RUN python -m venv /venv
ENV PATH="/venv/bin:$PATH"

WORKDIR /app
COPY ./requirements.txt requirements.txt

RUN pip install --upgrade pip \
    && pip install -r requirements.txt

COPY ./app.py .

CMD ["python", "app.py"]


EOF

```

&nbsp;


### Create logstash configuration file
- run command:

```
tee ~/elk/logstash.conf<<EOF
input {
    beats {
        port => 5044
    }
}

filter {
    grok {
        match => { "message" => "%{TIMESTAMP_ISO8601:timestamp} %{LOGLEVEL:loglevel}: %{GREEDYDATA:message}" }
    }
    date {
        match => [ "timestamp", "YYYY-MM-dd HH:mm:ss" ]
    }
}

output {
    elasticsearch {
        hosts => ["http://elasticsearch:9200"]
        index => "flask-logs-%{+YYYY.MM.dd}"
    }
    stdout {
        codec => rubydebug
    }
}

EOF

```

&nbsp;

### Create  filebeat configuration file
- run command:

```

tee ~/elk/filebeat.yml<<EOF
filebeat.inputs:
- type: log
  enabled: true
  paths:
    - /var/log/pythonapp/app.log

output.logstash:
  hosts: ["logstash:5044"]

EOF

```

&nbsp;


- run command:

```
 
 sudo chown 0:0 ~/elk/filebeat.yml
 sudo chmod go-w ~/elk/filebeat.yml

```


### Create kibana configuration file
 - run command:

```

tee ~/elk/kibana.yml<<EOF
elasticsearch.hosts: ["http://elasticsearch:9200"]
server.host: "0.0.0.0"

EOF

```

&nbsp;

### Create  docker-compose configuration file
- run command

```
tee ~/elk/docker-compose.yml<<EOF
version: '3.7'

services:
  elasticsearch:
    image: docker.elastic.co/elasticsearch/elasticsearch:7.9.3
    container_name: elk-elasticsearch
    environment:
      - discovery.type=single-node
    volumes:
      - ~/elk/esdata:/usr/share/elasticsearch/data
    user: "root"
    ports:
      - "9200:9200"

  logstash:
    image: docker.elastic.co/logstash/logstash:7.9.3
    container_name: elk-logstash
    environment:
      - "LS_JAVA_OPTS=-Xmx256m -Xms256m"
    volumes:
      - ~/elk/logstash.conf:/usr/share/logstash/pipeline/logstash.conf
    ports:
      - "5044:5044"
    depends_on:
      - elasticsearch

  kibana:
    image: docker.elastic.co/kibana/kibana:7.9.3
    container_name: elk-kibana
    ports:
      - "5601:5601"
    depends_on:
      - elasticsearch
    volumes:
      - ~/elk/kibana.yml:/usr/share/kibana/config/kibana.yml

  pythonapp:
    build: ~/elk/application
    container_name: elk-pythonapp
    volumes:
      - ~/elk/application:/app
      - ~/elk/applogs:/app/logs
    ports:
      - "5000:5000"
    deploy:
      resources:
        limits:
          memory: 200M

  filebeat:
    image: docker.elastic.co/beats/filebeat:7.9.3
    container_name: elk-filebeat
    volumes:
      - ~/elk/filebeat.yml:/usr/share/filebeat/filebeat.yml
      - ~/elk/applogs:/var/log/pythonapp
    depends_on:
      - logstash


EOF

```