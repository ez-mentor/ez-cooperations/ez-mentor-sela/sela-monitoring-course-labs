# ELK using Docker
# Lab 02: Get application logs & search elasticsearch for messages


---

# Tasks

 - Verify Application is up and running 

 - Call the application with REST API's

 - Get application log files
 
 - Verify Elasticsearch health 

 - Search Elasticsearch to get application logs

---

## Run ALL Applications

- run command on linux shell (It will eait few minutes as it needs to pull all images to local docker)

```
cd ~/elk
docker-compose up

```

- Open new linux shell


## Verify Application is up and running
- run command on linux shell

```

docker logs elk-pythonapp

```
- verify that you have this:

```

 * Serving Flask app "app" (lazy loading)
 * Environment: production

```

- run command on linux shell

```
ls -l ~/elk/applogs/app.log

```

- run comandon linux shell 
```
cat ~/elk/applogs/app.log

```

- verify that file exist and it has these lines:

```
   WARNING: This is a development server. Do not use it in a production deployment.
2024-01-31 16:32:37 INFO:  * Running on http://172.31.0.2:5000/ (Press CTRL+C to quit)
2024-01-31 16:32:37 INFO:  * Restarting with stat
2024-01-31 16:32:39 WARNING:  * Debugger is active!
2024-01-31 16:32:39 INFO:  * Debugger PIN: 956-590-599

```

### Call the application with REST API's

- run command on linux shell

```
 
 curl -X POST -H "Content-Type: application/json" -d '{"age": 5784, "name": "world" }' http://localhost:5000/data
 curl -X POST -H "Content-Type: application/json" -d '{}' http://localhost:5000/data

```
 
- show the contetent of the application log file

```
 cat  ~/elk/applogs/app.log

```



- Verify that these 2 lines exist in file ~/elk/applogs/app.log


```
2024-01-16 16:40:52 INFO: Received data: name=world, age=5784
2024-01-16 16:43:51 ERROR: NO data Received
```

<img alt="Image 1.1" src="images/application_logs_1.png" width="100%" height="100%">

&nbsp;

### Verify Elasticsearch health 

- Make sure to change <server ip> with the actual server ip address you are using

```

Browse to http://<server ip>:9200/

```

<img alt="Image 1.1" src="images/es_verification_1.png" width="100%" height="100%">


```

Browse to http://<server ip>:9200/_cluster/health

Verify that staus is yellow or green and it is ok


```

<img alt="Image 1.1" src="images/es_verification_health.png" width="100%" height="100%">



### Search Elasticsearch to get application logs

- Make sure to change <server ip> with the actual server ip address you are using

```

Browse to http://<server ip>:9200/flask-logs*/_search?q=message:"Received"

Verify that there is 2 hits 

" {"took":25,"timed_out":false,"_shards":{"total":2,"successful":2,"skipped":0,"failed":0},"hits":{"total":{"value":1,"relation":"eq"}" 

```
- - Find the 2 messages for Received Data

- in the image below there are 2 hits
- The messages are marked in blue box and the hits are mrked in red box

<img alt="Image 1.1" src="images/elasticsearch_search_2.png" width="100%" height="100%">

- Search elasticsearch using time slot and other filters
