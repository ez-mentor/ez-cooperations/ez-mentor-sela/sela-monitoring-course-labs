# ELK using Docker
# Lab 04: Logstash and compare to applications logs
  


---

# Tasks

 - logstash log files

 - understand the fields to be pushed to logstash

 - Open KIBANA DASHBAORD

 - compare the application log to Kibana Dashboard
 

---

### logstash log files


- These lines are from logstash config file
- Answer : What will be the elasticsearch index name ? _______

```
output {
    elasticsearch {
        hosts => ["http://elasticsearch:9200"]
        index => "flask-logs-%{+YYYY.MM.dd}"
    }
    stdout {
        codec => rubydebug
    }
}

```

 - run command:
 
```
docker logs elk-logstash

```

- each line from the log file is being cut to many fields in elasticsearch index
- the below are the fields based on logstash log file:

```
{
           "log" => {
          "file" => {
            "path" => "/var/log/pythonapp/app.log"
        },
        "offset" => 3575
    },
      "loglevel" => "INFO",
       "message" => [
        [0] "2024-01-17 16:44:08 INFO: 172.30.0.1 - - [17/Jan/2024 16:44:08] \"POST /data HTTP/1.1\" 200 -",
        [1] "172.30.0.1 - - [17/Jan/2024 16:44:08] \"POST /data HTTP/1.1\" 200 -"
    ],
          "host" => {
        "name" => "256fa493cf42"
    },
      "@version" => "1",
    "@timestamp" => 2024-01-17T16:44:08.000Z,
         "agent" => {
                "type" => "filebeat",
                "name" => "256fa493cf42",
                  "id" => "4832b9f7-e87a-4c2a-81ad-0182cb7d4041",
             "version" => "7.9.3",
        "ephemeral_id" => "3c826a7d-2839-4849-bd68-b40f1431cce4",
            "hostname" => "256fa493cf42"
    },
     "timestamp" => "2024-01-17 16:44:08",
          "tags" => [
        [0] "beats_input_codec_plain_applied"
    ],
           "ecs" => {
        "version" => "1.5.0"
    },
         "input" => {
        "type" => "log"
    }
}


```

### Open KIBANA DASHBAORD

- Make sure to change <server ip> with the actual server ip address you are using

- Browse to http://<server ip>:5601/app/home#/
- -> Dashboard -> DATA Dashboard

- On different window display the application log file 
- run command on linux shell

```
 cat  ~/elk/applogs/app.log

```

- Compare the messages in the log file to the Dashboard

- run commands on linux shell

 ```
 
  curl -X POST -H "Content-Type: application/json" -d '{"age": <your age>, "name": <your name> }' http://localhost:5500/data
  cat  ~/elk/applogs/app.log


 ```

- Find your name in the app log file
- Find the message with your name  and your age in the dashboard