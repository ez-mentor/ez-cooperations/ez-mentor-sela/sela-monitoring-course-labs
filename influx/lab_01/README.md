# Influxdb Telegraf and Grafana with Docker
# Lab 01: Create Influxdb Telegraf and Grafana configuration files

---

# Preparations
 - We will install Influxdb, Telegraf and Grafana on separate containers using docker and docker-compose

 - We will demonstrate metricsses coming from telegraf container
  
 - Get monitoring server IP (ask the instructor)
 
 - Credentials: 
 username:**sela** , password:**sela**

---

# Tasks

 - Make sure docker, docker-compose and curl are installed
 
 - Create persistent volume
 
 - Create  telegraf configuration file

 - Create  grafana configuration and datasource files

 - Create  docker-compose configuration file


--- 
 
## Make sure docker, docker-compose and curl are installed

&nbsp;

 - Inside the server open bash (git bash on windows) and run commands:

```
docker version
docker-compose version
curl --version

```
<img alt="Image 1.1" src="images/verification.png" width="100%" height="100%">

&nbsp;

 - Create persistent volume, run command:
 
```
mkdir -p ~/influx/grafana

```

&nbsp;

 - Create  telegraf configuration file
 - run command:
 - Make sure to change <server ip> with the actual server ip address you are using
```

tee ~/influx/telegraf.conf<<EOF
[agent]
  interval = "10s"

[[outputs.influxdb]]
  urls = ["http://influxdb:8086"]
  database = "telegraf"
  username = "admin"
  password = "admin1234"

[[inputs.cpu]]
  percpu = true
  totalcpu = true
  collect_cpu_time = false

[[inputs.mem]]

[[inputs.swap]]

EOF

```


&nbsp;


- Create  grafana configuration file, run commands:

```

  curl  https://raw.githubusercontent.com/grafana/grafana/v7.5.1/conf/defaults.ini -o ~/influx/grafana/grafana.ini
  sed -i s/3000/3010/ ~/influx/grafana/grafana.ini

```

&nbsp;

- Create  grafana configuration datasource file, run command:
- Make sure to change <server ip> with the actual server ip address you are using

```

tee ~/influx/grafana/datasource.yml<<EOF
apiVersion: 1
datasources:
- name: InfluxDB-Test
  type: InfluxDB
  url: http://<server ip>:8086 
  isDefault: true
  access: direct
  editable: true
EOF

```

&nbsp;
