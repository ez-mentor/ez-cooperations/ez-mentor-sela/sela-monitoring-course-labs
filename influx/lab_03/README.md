# Influxdb Telegraf and Grafana with Docker
# Lab 03: Grafana Dashboard using influxdb metrics


# Preparations
- Verify that Grafana up and running:
    - Browse to http://serverip_or_hostname:3010

---

# Tasks

 - create Datasource

 - cpu metrics

 - Grafana Dashboard

 --- 
 

## Create Datasource
- Navigate to grafana Home
- Create a Dashboard: Go to Datasource.
- Add InfluxDB  datasource - replace server-ip with your sever ip:
```
    http://<server-ip>:8086
    access: Browser
    Database: telegraf
    user: admin
    password: admin1234
```
- Save and test


## cpu metrics and Grafana Dashboard
- Navigate to grafana Home
- Create a Dashboard: Go to Grafana and create a new dashboard.
- Add a Panel: In the dashboard, add a new panel.

<img alt="Image 3" src="images/grafana_1.png" width="75%" height="100%">
<img alt="Image 4" src="images/grafana_2.png" width="75%" height="100%">

- Configure the Query: In the panel settings, 
- select InfluxDB as the data source and build INFLUXDB query. 
- You can select from the available measurements and choose fields to display.

<img alt="Image 5" src="images/grafana_3.png" width="75%" height="100%">

- Use Query: Select cpu measurement and choose to display field usage_user
- Add Another Query: Select cpu measurement and choose to display field usage_system


- Save and View: Save the panel and dashboard. 
- You should now be able to see the 2 metrics collected by Telegraf visualized in Grafana.

<img alt="Image 5" src="images/grafana_4.png" width="75%" height="100%">
<img alt="Image 5" src="images/grafana_5.png" width="75%" height="100%">




