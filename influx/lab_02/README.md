# Influxdb Telegraf and Grafana with Docker
# Lab 02: Create  docker-compose configuration filea and run the application


# Preparations
 - Verify that these files exist:
    - ~/influx/telegraf.conf
    - ~/influx/grafana/grafana.ini 
    - ~/influx/grafana/datasource.yml
    
---

# Tasks

 - Create  docker-compose configuration file

 - Create containers using docker-compose 

 - run the applications


--- 
 
## Create  docker-compose configuration file

&nbsp;

- run command:

```
tee ~/influx/docker-compose.yml << EOF
version: '3'
services:
  influxdb:
    image: influxdb:1.7
    container_name: influxdb
    ports:
      - "8086:8086"
    environment:
      - INFLUXDB_DB=telegraf
      - INFLUXDB_ADMIN_USER=admin
      - INFLUXDB_ADMIN_PASSWORD=admin1234
    volumes:
      - influx_influxdb-data:/var/lib/influxdb

  telegraf:
    image: telegraf:latest
    container_name: telegraf
    volumes:
      - ./telegraf.conf:/etc/telegraf/telegraf.conf:ro
    links:
      - influxdb
    depends_on:
      - influxdb
  grafana:
    image: grafana/grafana:7.5.17
    container_name: influx_grafana
    volumes:
      - ./grafana/grafana.ini:/etc/grafana/grafana.ini
      - ./grafana/datasource.yml:/etc/grafana/provisioning/datasources/datasource.yaml
      - ./grafana/dashboards:/etc/grafana/provisioning/dashboards
      - ./grafana/dashboards:/var/lib/grafana/dashboards
    ports:
      - 3010:3010
    links:
      - influxdb
volumes:
  influx_influxdb-data:

EOF

```

## Create containers using docker-compose 

&nbsp;

- Navigate to influx directory, run commands:

```

cd  ~/influx/

```

&nbsp;

- Run the applications with docker-compose, run commands:

```

docker-compose up -d

```

&nbsp;

## Verify output:

- run docker containers:

```
 docker ps | grep -e telegraf -e influxdb -e influx_grafana
```

- You shoul get 3 docker contaners

```
 5de4d00fbf22        grafana/grafana:7.5.17     "/run.sh"                13 minutes ago      Up 2 minutes        3000/tcp, 0.0.0.0:3010->3010/tcp   influx_grafana
 39ca130a7030        telegraf:latest            "/entrypoint.sh tele…"   15 hours ago        Up 15 hours         8092/udp, 8125/udp, 8094/tcp       telegraf
 bfa1fa66c069        influxdb:1.7               "/entrypoint.sh infl…"   15 hours ago        Up 15 hours         0.0.0.0:8086->8086/tcp             influxdb

 Take a note for the telegraf container is -> This will be the host in for the metric

```


&nbsp;

## run the applications

- Influxdb: 
- Login to the server and run command :

```
 docker exec -it influxdb influx
 USE telegraf
 SELECT * FROM cpu LIMIT 10

```

- Results will look like the below

```
>  SELECT * FROM cpu LIMIT 10
name: cpu
time                cpu       host         usage_guest usage_guest_nice usage_idle         usage_iowait        usage_irq usage_nice usage_softirq       usage_steal usage_system        usage_user
----                ---       ----         ----------- ---------------- ----------         ------------        --------- ---------- -------------       ----------- ------------        ----------
1706496090000000000 cpu-total 39ca130a7030 0           0                27.07070707057285  0                   0         0          0.05050505050493962 0           1.5151515151477222  71.36363636346908
1706496090000000000 cpu0      39ca130a7030 0           0                27.502527805705025 0                   0         0          0.10111223458083435 0           1.213346814970084   71.18301314490853
1706496090000000000 cpu1      39ca130a7030 0           0                26.71370967726793  0                   0         0          0                   0           1.7137096774318135  71.57258064569793
1706496100000000000 cpu-total 39ca130a7030 0           0                94.84742371229612  0.10005002501246187 0         0          0                   0           0.45022511255721587 4.602301150577796
1706496100000000000 cpu0      39ca130a7030 0           0                95.29058116259581  0.10020040080169909 0         0          0                   0           0.4008016032079355  4.208416833675918
1706496100000000000 cpu1      39ca130a7030 0           0                94.40559440562286  0.09990009990020003 0         0          0                   0           0.49950049950071623 4.995004995000348
1706496110000000000 cpu-total 39ca130a7030 0           0                98.60209685458038  0.09985022466277539 0         0          0                   0           0.499251123313877   0.7988017973022031
1706496110000000000 cpu0      39ca130a7030 0           0                98.5999999998603   0.1999999999998181  0         0          0                   0           0.49999999999954525 0.7000000000016371
1706496110000000000 cpu1      39ca130a7030 0           0                98.50448654015089  0                   0         0          0                   0           0.5982053838462442  0.8973080757716333
1706496120000000000 cpu-total 39ca130a7030 0           0                74.07221664966701  0.10030090270831422 0         0          0                   0           1.1033099297925968  24.724172517623405


```
